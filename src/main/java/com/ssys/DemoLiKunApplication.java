package com.ssys;

import com.honglang.commonbase.utils.ListUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class DemoLiKunApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoLiKunApplication.class, args);
        List<Integer> listA = Arrays.asList(1, 2, 3, 4, 5, 6, 7);
        List<Integer> listB = Arrays.asList(2, 4, 5, 6, 7, 8, 9);
        System.out.println(ListUtils.deDuplicationIntegerList(new
                ArrayList<>(listA), new ArrayList<>(listB)));

    }

}
